# videos synchros

Permet de regarder des vidéos ensemble

## Contribuer

### Installer les dépendances

```
cd server
npm install
```

### Configurer NGINX (si utilisé) :

Editer `/etc/nginx/sites-enabled/default` et y ajouter :

```
location /wg/ {
    proxy_pass http://localhost:9420;
    proxy_http_version 1.1;
    proxy_set_header Upgrade $http_upgrade;
    proxy_set_header Connection "Upgrade";
    proxy_set_header Host $host;
} 
```

### Si utilisation locale (sans serveur web) : 

Remplacer dans `client/script.js` 
`const sock = new WebSocket("wss://" + server_adress + "/wg/");` par
`const sock = new WebSocket("wss://" + server_adress + ":9420");`

## A faire

* Synchronisation avec le plus lent
* Synchronisation d'un nouveau client sur le temps actuel
* Améliorer le tchat --> css principalement et le rendre plus ergonomique
* Afficher les clients connectés
* Ajouter la gestion d'une playlist
* Mettre en place le fullscreen

