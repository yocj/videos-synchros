const Websocket = require('ws');
const fetch = require('node-fetch');
const wss = new Websocket.Server({ port: 9420, clientTracking: true });
let current_video = null;

const nop = () => {};

const ping = () => {
    wss.clients.forEach( (ws) => {
        if (!ws.is_alive) return ws.terminate();
        ws.is_alive = false;
        ws.ping(nop);
    });
};

const get_video = (vid_id) => {
    return fetch("https://invidious.bretzel.ga/api/v1/videos/" + vid_id)
        .then((res) => {
            if (res.ok) {
                return res;
            } else {
                return res.text()
                    .then((text) => {
                        throw new Error(text);
                    })
            }
        })
        .then((res) => {
            return res.json();
        })
        .catch((e) => {
            console.log(e);
        })
};

const get_id = () => {
    const o = () => {
        return Math.floor(Math.random() * 65536).toString(16);
    };
    return o() + "-" + o() + "-" + o() + "-" + o();
};

const broadcast = (data) => {
    wss.clients.forEach((client) => {
        client.send(JSON.stringify(data));
    });
};

const handle_message = (msg, sender) => {
    switch (msg.type) {
        case "play":
            console.log(msg.text);
            broadcast({ type: "play", id: sender });
            break;
        case "pause":
            console.log(msg.text);
            broadcast({ type: "pause", id: sender });
            break;
        case "video_id":
            get_video(msg.id)
                .then((data) => {
                    //let url = data.adaptiveFormats[0].url;//Format sans son
                    let url = data.formatStreams[0].url; //Format avec son
                    let title = data.title;
                    let desc = data.descriptionHtml;
                    let duration = data.lengthSeconds;
                    let response = {
                        type: "video",
                        vidinfos: [url, title, desc, duration],
                        id: sender
                    };
                    console.log("Sending video...");
                    current_video = response;
                    broadcast(response);
                })
                .catch((err) => {
                    console.log(err);
                })
            break;
        case "seek":
            let time = msg.value;
            console.log("Seeking to " + time + "s...");
            broadcast({ type: "seek", value: time, id: sender });
            break;
        case "chat":
            const response = {
                type: "chat",
                text: msg.text,
                id: sender
            };
            broadcast(response);
            break;
    }
};

const main = () => {
    console.log("server started");

    wss.on('connection', (ws) => {
        ws.client_id = get_id();
        ws.is_alive = true;
        console.log("client connected with id " + ws.client_id);

        if (current_video !== null) {
            ws.send(JSON.stringify(current_video));
        }

        ws.on('message', (message) => {
            let msg = JSON.parse(message);
            console.log("client with id " + ws.client_id + " is sending data...");
            handle_message(msg, ws.client_id);

        });

        ws.on('pong', () => {
            ws.is_alive = true;
        });
    });


    const interval = setInterval(ping, 30000);

    wss.on('close', () => {
        clearInterval(interval);
    });

    wss.on('error', (err) => {
        console.log(err);
    });

};

main();